FROM mhart/alpine-node

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
COPY gulpfile.js /usr/src/app/
COPY API-Spec.yaml /usr/src/app/
RUN npm install --production
COPY src /usr/src/app/src/
COPY data /usr/src/app/data/
EXPOSE 3000
CMD [ "npm", "start" ]