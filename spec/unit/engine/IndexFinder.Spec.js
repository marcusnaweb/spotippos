'use strict';
let expect = require('expect');
let IndexFinder = require('../../../src/engine/Index/IndexFinder');

describe('Busca por um grupo de índíces.', function () {
    it('Deve buscar pelo indice especificado no indice correto.', function () {
        let fakeIdIndex = {
            field: 'id',
            find() {}
        };

        let fakeBathsIndex = {
            field: 'baths',
            find() {}
        };

        let fakeIdIndexSpy = expect.spyOn(fakeIdIndex, 'find');
        let fakeBathsIndexSpy = expect.spyOn(fakeBathsIndex, 'find');

        let indexFinder = new IndexFinder([fakeIdIndex, fakeBathsIndex]);

        indexFinder.find(1, 'id');
        expect(fakeIdIndexSpy).toHaveBeenCalledWith(1);
        expect(fakeBathsIndexSpy).toNotHaveBeenCalled();
    });

    it('Deve buscar por todos os indices disponiveis caso o parametro de indice não seja fornecido.', function () {
        let fakeIdIndex = {
            field: 'id',
            find() {}
        };

        let fakeBathsIndex = {
            field: 'baths',
            find() {}
        };

        let fakeIdIndexSpy = expect.spyOn(fakeIdIndex, 'find');
        let fakeBathsIndexSpy = expect.spyOn(fakeBathsIndex, 'find');

        let indexFinder = new IndexFinder([fakeIdIndex, fakeBathsIndex]);

        indexFinder.find(1);
        expect(fakeIdIndexSpy).toHaveBeenCalledWith(1);
        expect(fakeBathsIndexSpy).toHaveBeenCalledWith(1);
    });
});