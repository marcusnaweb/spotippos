'use strict';
let expect = require('expect');
let MapFinder = require('../../../src/engine/Map/MapFinder');

describe('Busca por um grupo de mapas.', function () {
    it('Deve encontrar um mapa que comporte a zona.', function () {
        let zoneBoundsMock = Symbol('MapFinderTest');

        let rightMapMock = {
            contains(zoneBounds) { return zoneBounds === zoneBoundsMock; }
        };

        let wrongMapMock = {
            contains(zoneBounds) { return zoneBounds !== zoneBoundsMock }
        };

        let mapFinder = new MapFinder([wrongMapMock, rightMapMock]);

        expect(mapFinder.find(zoneBoundsMock)).toBe(rightMapMock);
    });

    it('Deve falhar quando nenhum mapa for encontrado pois significa que a busca está fora do limite máximo configurado.', function () {
        let mapMock = {
            contains(zoneBounds) { return false; }
        };

        let mapFinder = new MapFinder([mapMock]);

        expect(() => mapFinder.find(zoneBoundsMock)).toThrow();
    });
});