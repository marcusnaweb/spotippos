let expect = require('expect');
let proxyquire = require('proxyquire');

let IndexFinder = require('../../../src/engine/Index/IndexFinder');
let Map = require('../../../src/models/Map');
let MapFinder = require('../../../src/engine/Map/MapFinder');
let Property = require('../../../src/models/Property');
let config = require('config');


describe('Motor de busca', function () {
    let findByIdSpy = expect.createSpy();
    let findByZoneSpy = expect.createSpy();
    let createSubZoneSpy = expect.createSpy();

    let SearchEngine = proxyquire('../../../src/engine/Search/SearchEngine', {
        '../../models/Index': class {
            fromProperties() {
                return [];
            }
        },
        '../Index/IndexFinder': class {
            find(id, field) {
                return findByIdSpy(id, field);
            }
        },
        '../Map/MapFinder': class {
            find(zoneBounds) {
                findByZoneSpy(zoneBounds);
                return {
                    createSubZone: function (zoneBounds) {
                        createSubZoneSpy(zoneBounds);
                    }
                };
            }
        },
        '../../models/Property': class {
            static loadSync() {
                return [];
            }

            static load() {
                return Promise.resolve([]);
            }
        },
        'config': {
            get() { return ''; }
        }
    });

    it('Deve fazer buscas pelo id da propriedade.', function () {
        let searchEngine = new SearchEngine();
        searchEngine.findById(5);
        expect(findByIdSpy).toHaveBeenCalledWith(5, 'id');
    });

    it('Deve fazer buscas pela zona.', function () {
        let searchEngine = new SearchEngine();
        const testObj = {};
        searchEngine.findByZone(testObj);
        expect(findByZoneSpy).toHaveBeenCalledWith(testObj);
    });

    it('Reload deve recriar os finders e reconsultar as propriedades "assincronamente".', function () {
        let mapFinderCtorSpy = expect.createSpy();
        let indexFinderCtorSpy = expect.createSpy();
        let propertyArrayAsync = [];

        let SearchEngine = proxyquire('../../../src/engine/Search/SearchEngine', {
            '../../models/Index': class {
                fromProperties() {
                    return [];
                }
            },
            '../Map/MapFinder': mapFinderCtorSpy,
            '../Index/IndexFinder': indexFinderCtorSpy,
            '../../models/Property': class {
                static loadSync() {
                    return [];
                }

                static load() {
                    return Promise.resolve(propertyArrayAsync);
                }
            },
            'config': {
                get() { return ''; }
            }
        });
        
        new SearchEngine().reload();

        expect(indexFinderCtorSpy).toHaveBeenCalled();
        expect(mapFinderCtorSpy).toHaveBeenCalled();
    });
});