'use strict';
let ZoneBounds = require('../../../src/models/ZoneBounds');
let expect = require('expect');

describe('Uma porção do mapa de tamanho arbitrário', function () {
    it('Deve indicar que uma determinada area está contida EXATAMENTE na mesma area de outra zona.', function () {
        let zoneBounds = new ZoneBounds(0, 100, 100, 0);
        expect(zoneBounds.contains(new ZoneBounds(0, 100, 100, 0))).toBe(true);
    });

    it('Deve indicar que uma determinada area está contida nessa zona.', function () {
        let zoneBounds = new ZoneBounds(0, 100, 100, 0);
        expect(zoneBounds.contains(new ZoneBounds(0, 100, 100, 0))).toBe(true);
    });

    it('Deve indicar que uma determinada area está na borda de outra.', function () {
        let zoneBounds = new ZoneBounds(0, 100, 100, 0);
        expect(zoneBounds.contains(new ZoneBounds(50, 50, 100, 100))).toBe(true);
    });

    it('Deve indicar que uma determinada area está fora de outra.', function () {
        let zoneBounds = new ZoneBounds(0, 100, 100, 0);
        expect(zoneBounds.contains(new ZoneBounds(0, 101, 101, 0))).toBe(false);
    });
});