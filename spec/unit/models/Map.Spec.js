'use strict';
let Map = require('../../../src/models/Map');
let expect = require('expect');
let proxyquire = require('proxyquire');
const defaultMapX = 1400;
const defaultMapY = 1000;

describe('Um plano bidimensional subdividido por zonas.', function () {
    it('Deve criar n mapas baseado no arquivo de configuração.', function () {
        expect(Map.fromProperties([], 5, 1400, 1000).length).toBe(5);
    });

    it('Deve verificar se um ZoneBounds cabe em alguma das zonas do mapa.', function () {
        let zoneBoundsMock = Symbol('MapFinder');

        let ProxiedMap = proxyquire('../../../src/models/Map', {
            './Zone': class ZoneMock {
                contains(zoneBounds) {
                    return zoneBounds === zoneBoundsMock;
                }

                static splitMap() {
                    return [new ZoneMock()];
                }
            }
        });

        const zonesCount = 2;

        expect(new ProxiedMap(defaultMapX, defaultMapY, zonesCount, []).contains(zoneBoundsMock)).toBe(true);
        expect(new ProxiedMap(defaultMapX, defaultMapY, zonesCount, []).contains(Symbol())).toBe(false);
    });
});