'use strict';
let expect = require('expect');
let proxyquire = require('proxyquire');
let Province = require('../../../src/models/Province');

const provinceMock = {
    "Gode": {
        "boundaries": {
            "upperLeft": {
                "x": 0,
                "y": 1000
            },
            "bottomRight": {
                "x": 600,
                "y": 500
            }
        }
    }
};

let ProxyedProvince = proxyquire('../../../src/models/Province', {
    '../storage/Local': class {
        static loadSync() {
            return provinceMock;
        }
    }
});

describe('Carregador de provincias.', function () {
    it('Deve carregar e transformar um plainObject em uma instancia de Province.', function () {
        let province = ProxyedProvince.loadSync()[0];
        expect(province.__proto__.constructor.name).toBe('Province');
    });

    it('O formato JSON de uma Province deve ser o nome da Provincia.', function () {
        const expectedName = 'Teste';
        let province = new Province(1, 1, 1, 1, expectedName);
        expect(JSON.stringify(province)).toBe(`"${expectedName}"`);
    });
});
