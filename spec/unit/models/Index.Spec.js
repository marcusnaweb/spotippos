'use strict';
let expect = require('expect');
let Index = require('../../../src/models/Index');
let fs = require('fs');
let path = require('path');
let propertiesMock = JSON.parse(fs.readFileSync(path.join(__dirname, '../mocks/propertiesMock.json'), 'utf8'));

describe('Indexador de propriedades por um atributo arbitrário.', function () {
    it('Deve criar um indice a partir do campo id.', function () {
        let index = new Index(propertiesMock.properties, 'id');
        expect(index.find(propertiesMock.properties[0].id)[0]).toBe(propertiesMock.properties[0]);
    });

    it('Deve falhar ao tentar modificar um nó do indice.', function () {
        let index = new Index(propertiesMock.properties, 'id');
        expect(() => index.find(1).push({})).toThrow();
    });

    it('Deve falhar ao tentar modificar um indice.', function () {
        let index = new Index(propertiesMock.properties, 'id');
        expect(() => index['lugarqualquer'] = {}).toThrow();
    });

    it('Deve agrupar multiplas propriedades para indices identicos.', function () {
        let index = new Index(propertiesMock.properties, 'baths');
        expect(index.find(4).length).toBe(2);
    });

    it('Deve falhar ao tentar criar indice por campo inexistente.', function () {
        expect(() => new Index(propertiesMock.properties, 'pepper')).toThrow();
    });
});