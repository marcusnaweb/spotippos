'use strict';
let Zone = require('../../../src/models/Zone');
let ZoneBounds = require('../../../src/models/ZoneBounds');
let Property = require('../../../src/models/Property');
let proxyquire = require('proxyquire');
let expect = require('expect');
const defaultMapX = 1400;
const defaultMapY = 1000;
describe('Subdivisão de um mapa.', function () {
    it('Deve falhar se limites não forem oferecidos a zona.', function () {
        expect(() => new Zone(undefined, undefined, undefined, undefined, [])).toThrow();
    });

    it('Deve falhar se propriedades não forem oferecidas a zona.', function () {
        expect(() => new Zone(undefined, undefined, undefined, undefined, null)).toThrow();
    });

    it('Deve conter apenas as propriedades que fazem parte da area em que a zona se encontra.', function () {
        let insideBoundsProperty = new Property({
            'lat': 55,
            'long': 60
        }, []);

        let outsideBoundsProperty = new Property({
            'lat': 120,
            'long': 200
        }, []);

        let zone = new Zone(0, 100, 100, 0, [insideBoundsProperty, outsideBoundsProperty]);
        expect(zone.properties.length).toBe(1);
        expect(zone.properties[0]).toBe(insideBoundsProperty);
    });

    it('Deve criar uma zona menor a partir de uma zona especifica.', function () {
        let insideBoundsProperty = new Property({
            'lat': 50,
            'long': 50
        }, []);

        let outsideBoundsProperty = new Property({
            'lat': 99,
            'long': 99
        }, []);

        let zone = new Zone(0, 100, 100, 0, [insideBoundsProperty, outsideBoundsProperty]);
        let subZone = zone.createSubZone(0, 51, 51, 0);

        expect(subZone.properties.length).toBe(1);
        expect(subZone.properties[0]).toBe(insideBoundsProperty);
    });

    it('Deve criar um mapa com zona X: 1400, Y: 1000', function () {
        let zoneBoundSpy = expect.createSpy();
        let ProxiedZone = proxyquire('../../../src/models/Zone', {
            './Area': zoneBoundSpy
        });
        const zonesCount = 1;

        ProxiedZone.splitMap({ sizeX: defaultMapX, sizeY: defaultMapY}, zonesCount, []);
        expect(zoneBoundSpy).toHaveBeenCalledWith(0, 1000, 1400, 0);
    });

    it('Deve criar um mapas com duas zonas sendo a primeira AX: 0, AY: 1000, BX: 700, BY: 0 e a segunda AX: 700, AY: 1000, BX: 1400, BY: 0', function () {
        let zoneBoundSpy = expect.createSpy();
        let ProxiedZone = proxyquire('../../../src/models/Zone', {
            './Area': zoneBoundSpy
        });
        const zonesCount = 2;

        ProxiedZone.splitMap({ sizeX: defaultMapX, sizeY: defaultMapY}, zonesCount, []);
        expect(zoneBoundSpy).toHaveBeenCalledWith(0, 1000, 700, 0);
        expect(zoneBoundSpy).toHaveBeenCalledWith(700, 1000, 1400, 0);
    });
});