'use strict';
let expect = require('expect');
let ZoneBounds = require('../../../src/models/ZoneBounds');
let fs = require('fs');
let path = require('path');
let proxyquire = require('proxyquire');
let propertiesMock = JSON.parse(fs.readFileSync(path.join(__dirname, '../mocks/propertiesMock.json'), 'utf8'));
let LocalStorageMock = class {
    static load() {
        return Promise.resolve(propertiesMock);
    }
    static save() {
        return Promise.resolve();
    }
};

let Property = proxyquire('../../../src/models/Property', {
    '../storage/Local': LocalStorageMock,
    './Province': class {
        static load() {
            return Promise.resolve([]);
        }
    }
});


describe('Um ponto em um lugar qualquer de um plano.', function () {
    it('Deve descobrir se a propriedade está dentro de um determinado limite.', function () {
        let property = new Property({
            "lat": 1257,
            "long": 928,
        }, []);

        expect(property.isIn(new ZoneBounds(0, 1000, 1400, 0))).toBe(true);
    });

    it('Deve descobrir se a propriedade está fora de um determinado limite.', function () {
        let property = new Property({
            "lat": 1401,
            "long": 680,
        }, []);

        expect(property.isIn(new ZoneBounds(0, 0, 1400, 1000))).toBe(false);
    });

    it('Deve carregar todas as propriedades do sistema.', function () {
        return Property.load('').then(properties => {
            expect(properties.length).toBe(propertiesMock.properties.length);
        });
    });

    it('As propriedades devem ter herança prototipal de Property.', function () {
        return Property.load('').then(properties => {
            expect(properties.every(property => property instanceof Property)).toBe(true);
        });
    });

    it('Deve atualizar o id da propriedade inserida.', function () {
        return Property.insert({}, '', '').then(property => {
            expect(property.id).toBe(propertiesMock.properties.length);
        });
    });
});