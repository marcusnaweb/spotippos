'use strict';
let expect = require('expect');
let Checks = require('../../../src/utils/Checks');

describe('Classe utilitária para testes de integridade de argumentos para métodos.', function () {
    describe('requireNonNull', function () {
        it('Deve lançar um Error quando o parametro oferecido for nulo.', function () {
            expect(() => Checks.requireNonNull(null, 'null')).toThrow();
        });

        it('Não deve lançar um Error quando o parâmetro oferecido não for nulo.', function () {
            expect(() => Checks.requireNonNull({}, 'plain obj')).toNotThrow();
        });
    });

    describe('requirePositive', function () {
        it('Deve lançar um Error quando o parametro oferecido for NaN.', function () {
            expect(() => Checks.requirePositive(NaN, 'NaN')).toThrow();
        });

        it('Não deve lançar um Error quando o parâmetro oferecido não for uma instancia de Number.', function () {
            expect(() => Checks.requirePositive('5', '5')).toThrow();
        });

        it('Não deve lançar um Error quando o parâmetro oferecido for uma instancia de Number positiva.', function () {
            expect(() => Checks.requirePositive(5, '5')).toNotThrow();
        });

        it('Deve lançar um Error quando o parâmetro oferecido for uma instancia de Number <= 0.', function () {
            expect(() => Checks.requirePositive(-5, '-5')).toThrow();
        });
    });

    describe('requireArray', function () {
        it('Deve lançar um Error quando o parametro oferecido for nulo.', function () {
            expect(() => Checks.requireArray(null, 'null')).toThrow();
        });

        it('Não deve lançar um Error quando o parâmetro oferecido não for nulo.', function () {
            expect(() => Checks.requireArray([], 'array')).toNotThrow();
        });
    });

    describe('instanceOf', function () {
        it('Deve lançar um Error quando o parametro oferecido não for uma instância do tipo oferecido.', function () {
            expect(() => Checks.instanceOf(new Number(5), String)).toThrow();
        });

        it('Não deve lançar um Error quando o parametro oferecido corresponder ao tipo oferecido.', function () {
            expect(() => Checks.instanceOf(new Number(5), Number)).toNotThrow();
        });
    });
});
