'use strict';
let expect = require('expect');
let RouteLoader = require('../../../src/utils/RouteLoader');

describe('Carregador das rotas Express.JS', function () {
    it('Deve carregar uma lista de roteadores no formato glob', function () {
        let routes = RouteLoader.load(['./fakeRouters/**/*.js']);
        expect(routes.length).toBe(2);
        expect(routes.every(route => /Baz|inga/g.test(route.router))).toBe(true);
    });
    
    it('Os nomes das rotas devem ser o nome do arquivo sem a extensão.', function () {
        let routes = RouteLoader.load(['./fakeRouters/**/*.js']);
        expect(routes.every(router => /^\/fakeRouter(\d?)$/g.test(router.path))).toBe(true);
    });
})