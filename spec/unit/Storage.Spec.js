'use strict';
let expect = require('expect');
let proxyquire = require('proxyquire');
let pathUtils = require('path');
const propertiesMock = JSON.stringify({
    "totalProperties": 1,
    "properties": [
        {
            "id": 1,
            "title": "Imóvel código 1, com 3 quartos e 2 banheiros.",
            "price": 643000,
            "description": "Laboris quis quis elit commodo eiusmod qui exercitation. In laborum fugiat quis minim occaecat id.",
            "lat": 1257,
            "long": 928,
            "beds": 3,
            "baths": 2,
            "squareMeters": 61
        }]
});

describe('Um container de dados que emula uma camada de persistência.', function () {
    describe('Assincronos', function () {
        it('Deve conseguir carregar um arquivo armazenado em disco.', function () {
            let Storage = proxyquire('../../src/storage/Local', {
                'fs': {
                    readFile(path, encoding, callback) {
                        setImmediate(callback, null, propertiesMock);
                    }
                }
            });

            return Storage.load('').then(data => {
                expect(data.totalProperties).toBe(1);
            });
        });

        it('Deve falhar quando a operação de leitura do disco falhar.', function () {
            let error = new Error('ENOTFOUND');
            let Storage = proxyquire('../../src/storage/Local', {
                'fs': {
                    readFile(path, encoding, callback) {
                        setImmediate(callback, error);
                    }
                }
            });

            return Storage.load('').then(data => {
                expect.fail("Esperada rejeição da promise.");
            }, err => {
                expect(err).toBe(error);
            });
        });

        it('Deve persistir objeto no arquivo especificado.', function () {
            const path = '/dummy/location';
            const expectedPath = pathUtils.join(process.cwd(), '/dummy/location');

            let Storage = proxyquire('../../src/storage/Local', {
                'fs': {
                    writeFile(path, data, callback) {
                        expect(typeof data).toBe('string');
                        expect(path).toBe(expectedPath);
                        setImmediate(callback, null);
                    }
                }
            });

            return Storage.save(path, JSON.parse(propertiesMock))
        });

        it('Deve falhar quando a operação de gravação no disco falhar.', function () {
            let error = new Error('EACCESS');
            const expectedPath = '/dummy/location';
            let Storage = proxyquire('../../src/storage/Local', {
                'fs': {
                    writeFile(path, data, callback) {
                        setImmediate(callback, error);
                    }
                }
            });

            return Storage.save('', JSON.parse(propertiesMock))
                .then(() => expect.fail('Esperada rejeição da promise.'),
                err => {
                    expect(err).toBe(error);
                });
        });
    });

    describe('Sincronos', function () {
        it('Deve ler arquivo de forma sincrona e estática.', function () {
            const path = '/dummy/location';

            let Storage = proxyquire('../../src/storage/Local', {
                'fs': {
                    readFileSync(path, encoding, callback) {
                        return propertiesMock;
                    }
                }
            });

            let data = Storage.loadSync(path);
            expect(data.totalProperties).toBe(1);
        });

        it('Deve falhar quando a leitura sincrona e estática falhar.', function () {
            const path = '/dummy/location';

            let Storage = proxyquire('../../src/storage/Local', {
                'fs': {
                    readFileSync(path, encoding, callback) {
                        throw new Error('NOTFOUND');
                    }
                }
            });

            expect(() => Storage.loadSync(path)).toThrow('NOTFOUND');
        });
    });
});