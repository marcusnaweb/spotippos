'use strict';
let fs = require('fs');
let path = require('path');
let Checks = require('../utils/Checks');

class LocalStorage {
    static load(filepath) {
        Checks.requireNonNull(filepath, 'filepath');
        filepath = resolveFilePath(filepath);
        return new Promise((resolve, reject) => {
            fs.readFile(filepath, 'utf8', (err, str) => {
                if (err)
                    reject(err);

                try {
                    resolve(JSON.parse(str));
                } catch (error) {
                    reject(error);
                }
            });
        });
    }

    static save(filepath, data) {
        Checks.requireNonNull(filepath, 'filepath');
        filepath = resolveFilePath(filepath);
        
        return new Promise((resolve, reject) => {
            fs.writeFile(filepath, JSON.stringify(data), (err) => {
                if (err)
                    reject(err);

                try {
                    resolve();
                } catch (error) {
                    reject(error);
                }
            });
        });
    }

    static loadSync(filepath) {
        let data = fs.readFileSync(filepath, 'utf8');
        return JSON.parse(data);
    }
}

function resolveFilePath(filepath) {
    return path.join(process.cwd(), filepath);
}

module.exports = LocalStorage;