'use strict';
let Checks = require('../../utils/Checks');

class MapFinder {
    constructor(maps) {
        this.maps = maps;
        Object.freeze(this);
    }

    find(zoneBounds) {
        let map = this.maps.find(map => map.contains(zoneBounds));

        if(!map)
            throw new Error('Nenhum mapa foi encontrado para esta área.');

        return map;
    }
}

module.exports = MapFinder;