'use strict';

class IndexFinder {
    constructor(indexArray) {
        this.indexes = createIndexByField(indexArray);
        Object.freeze(this);
    }

    find(key, indexKey) {
        if (indexKey !== null && indexKey !== undefined)
            return this.indexes[indexKey].find(key);
        
        for(let indexKey in this.indexes) {
            let result = this.indexes[indexKey].find(key);
            
            if(result)
                return result;
        }

        return undefined;
    }
}

function createIndexByField(indexesArray) {
    let indexes = {};

    indexesArray.forEach((index) => {
        indexes[index.field] = index;
    });

    return indexes;
}

module.exports = IndexFinder;