'use strict';
let Index = require('../../models/Index');
let IndexFinder = require('../Index/IndexFinder');
let Map = require('../../models/Map');
let MapFinder = require('../Map/MapFinder');
let Property = require('../../models/Property');
let config = require('config');
let Checks = require('../../utils/Checks');

class SearchEngine {
    constructor() {
        let properties = Property.loadSync(config.get('property.storage.uri'), config.get('province.storage.uri'));
        this.createIndexFinder(properties);
        this.createMapFinder(properties);
    }

    createMapFinder(properties) {
        let maps = Map.fromProperties(properties, config.get('search.mapsCount'), config.get('maps.sizeX'), config.get('maps.sizeY'));
        this.mapFinder = new MapFinder(maps);
        return properties;
    }

    createIndexFinder(properties) {
        let indexes = Index.fromProperties(properties, ['id']);
        this.indexFinder = new IndexFinder(indexes);
        return properties;
    }

    reload() {
        return Property.load(config.get('property.storage.uri'), config.get('province.storage.uri'))
            .then(this.createIndexFinder.bind(this))
            .then(this.createMapFinder.bind(this));
    }

    findById(id) {
        Checks.requireNonNull(id, 'id');
        let properties = this.indexFinder.find(id, 'id');

        if(properties)
            return properties[0];

        return undefined;
    }

    findByZone(zoneBounds) {
        Checks.requireNonNull(zoneBounds, 'zoneBoundsid');
        
        let map = this.mapFinder.find(zoneBounds);
        return map.createSubZone(zoneBounds);
    }
}

module.exports = SearchEngine;