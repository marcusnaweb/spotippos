'use strict';
let express = require('express');
let config = require('config');
let router = express.Router();
let SearchEngine = require('../engine/Search/SearchEngine');
let ZoneBounds = require('../models/ZoneBounds');
let Property = require('../models/Property');
let apicache = require('apicache');
let cacheOptions = config.get('api.cacheOptions');
let searchEngine = new SearchEngine();
configureAPICache();

router.get('/:id', function (req, res) {
    let properties = searchEngine.findById(req.params.id, 'id');

    if (properties)
        res.send(properties);
    else
        res.status(404).send(properties);
});

router.get('/', apicache(config.get('api.cacheOptions.expiration')), function (req, res) {
    let zoneBounds = new ZoneBounds(req.query.ax, req.query.ay, req.query.bx, req.query.by);
    res.send(searchEngine.findByZone(zoneBounds));
});

router.post('/', function (req, res) {
    Property.insert(req.body, config.get('property.storage.uri'),
        config.get('province.storage.uri')).then(property => {
            searchEngine.reload();
            res
                .header('Location', property.toResourceLocation(config.get('api.http')))
                .status(201)
                .send(property);
        }, genericAsyncErrorHandler(res))
});

function genericAsyncErrorHandler(res) {
    return function handler(error) {
        res.status(500).send(err);
    }
}

function configureAPICache() {
    if (cacheOptions.enabled) {
        cacheOptions.client = require('redis').createClient(config.get('redis'))
    }
 
    apicache = apicache.options(cacheOptions).middleware;
}

module.exports = router;
