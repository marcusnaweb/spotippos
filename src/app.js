'use strict';
let config = require('config');
require('appmetrics-statsd').StatsD(config.get('api.telemetry.statsD'));
let RouteLoader = require('./utils/RouteLoader');
let express = require('express');
let http = require('http');
let cors = require('cors');
let gzip = require('compression');
let app = express();
const port = config.get('api.http.port');
let bodyParser = require('body-parser');
var statsd = require('express-statsd');
var middleware = require('swagger-express-middleware');

middleware('../API-Spec.yaml', app, function (err, middleware) {
    app.use(
        statsd(config.get('api.telemetry.statsD')),
        middleware.metadata(),
        middleware.CORS(),
        middleware.parseRequest(),
        middleware.validateRequest(),
        bodyParser.json(),
        cors(config.get('api.corsOptions')),
        gzip(),
        errorHandler
    );

    registerRoutes();
    listen();
});

function registerRoutes() {
    let routes = RouteLoader.load(config.get('api.routespath'));

    routes.forEach(function (route) {
        app.use(route.path, route.router);
    });
}

function listen() {
    http.createServer(app)
        .on('listening', () => console.log('Servidor ouvindo na porta ' + port))
        .listen(port);
}

function errorHandler (err, req, res, next) {
    if(process.env.NODE_ENV !== 'development')
        Object.assign(err, { stack: null })
    
    return res.status(500).send(err);
}