'use strict';
class ZoneBounds {
    constructor(ax, ay, bx, by) {
        this.ax = ax;
        this.ay = ay;
        this.bx = bx;
        this.by = by;
        Object.freeze(this);
    }

    contains(zoneBounds) {
        return this.ax <= zoneBounds.ax &&
            this.bx >= zoneBounds.bx &&
            this.ay >= zoneBounds.ay &&
            this.by <= zoneBounds.by;
    }
}

module.exports = ZoneBounds;