'use strict';
let deepFreeze = require('deep-freeze');
let Checks = require('../utils/Checks');

class Index {
    constructor(properties, field) {
        this.field = field;
        this.container = createIndexByField(field, properties);
        deepFreeze(this);
    }

    static fromProperties(properties, fields) {
        Checks.requireArray(fields, 'fields');
        Checks.requireArray(properties, 'properties');
    
        return fields.map(field => new Index(properties, field));
    }

    find(key) {
        return this.container[key];
    }
}

//Semanticamente, os métodos index e add pertencem a classe Index.
//Porém a idéia desta implementação é não expor a forma como os indices são criados.
function createIndexByField(field, properties) {
    let index = {};
    
    properties.forEach(property => {
        add(index, field, property);
    });
    
    return index;
}

function add(index, field, property) {
    if (!property.hasOwnProperty(field))
        throw new Error(`O campo ${field} não existe.`);

    index[property[field]] = index[property[field]] || [];
    index[property[field]].push(property);
}

module.exports = Index;