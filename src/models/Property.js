'use strict';
let Checks = require('../utils/Checks');
let Storage = require('../storage/Local');
let Province = require('./Province');

class Property {
    constructor(defaultProperties, provinces) {
        if (defaultProperties)
            Object.assign(this, defaultProperties);

        this.provinces = provinces.filter(province => this.isIn(province.zoneBounds))
    }

    isIn(zoneBounds) {
        return this.lat >= zoneBounds.ax &&
            this.lat <= zoneBounds.bx &&
            this.long >= zoneBounds.by &&
            this.long <= zoneBounds.ay
    }

    toResourceLocation(endpoint) {
        return `http://${endpoint.host}:${endpoint.port}/properties/${this.id}`
    }

    static load(storageURI, provinceStorageURI) {
        return Promise.all([Storage.load(storageURI), Province.load(provinceStorageURI)])
            .then(([{ properties }, provinces]) => Property.parseRawObject(properties, provinces))
    }

    static count(storageURI) {
        return Storage.load(storageURI)
            .then(data => data.properties.length);
    }

    static loadSync(storageURI, provinceStorageURI) {
        let { properties } = Storage.loadSync(storageURI);
        let provinces = Province.loadSync(provinceStorageURI);
        return Property.parseRawObject(properties, provinces);
    }

    static parseRawObject(rawProperties, provinces) {
        return rawProperties
            .map(rawProperty => new Property(rawProperty, provinces));
    }

    static insert(property, storageURI, provinceStorageURI) {
        return Promise.all([Storage.load(storageURI), Province.load(provinceStorageURI)])
            .then(([data, provinces]) => {
                data.properties.push(property);
                property.id = data.properties.length;
                return Storage.save(storageURI, data).then(() => new Property(property, provinces));
            });
    }
}

module.exports = Property;