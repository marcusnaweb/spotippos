'use strict';
let ZoneBounds = require('./ZoneBounds');
let Checks = require('../utils/Checks');

class Area {
    constructor(ax, ay, bx, by) {
        Checks.requireNumber(ax, 'ax');
        Checks.requireNumber(ay, 'ay');
        Checks.requireNumber(bx, 'bx');
        Checks.requireNumber(by, 'by');
        
        this.zoneBounds = new ZoneBounds(ax, ay, bx, by);
    }

    contains(zoneBounds) {
        return this.zoneBounds.contains(zoneBounds);
    }
}

module.exports = Area;