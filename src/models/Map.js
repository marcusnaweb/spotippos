'use strict';
let Zone = require('./Zone');
let Checks = require('../utils/Checks');

class Map {
    constructor(sizeX, sizeY, zonesCount, properties) {
        Checks.requirePositive(sizeX, 'sizeX');
        Checks.requirePositive(sizeY, 'sizeY');
        Checks.requirePositive(zonesCount, 'zonesCount');
        Checks.requireArray(properties, 'properties');

        this.sizeX = sizeX;;
        this.sizeY = sizeY;
        this.zones = Zone.splitMap(this, zonesCount, properties);
        Object.freeze(this);
    }

    contains(zoneBounds) {
        return this.zones.some(zone => zone.contains(zoneBounds));
    }

    createSubZone(zoneBounds) {
        if (!this.contains(zoneBounds))
            throw new Error('Esta zona não está contida neste mapa.');

        return this.zones.find(zone => zone.contains(zoneBounds))
            .createSubZone(zoneBounds.ax, zoneBounds.ay, zoneBounds.bx, zoneBounds.by);
    }

    static fromProperties(properties, mapsCount, sizeX, sizeY) {
        let maps = [];

        for (var i = 1; i <= mapsCount; i++) {
            maps.unshift(new Map(sizeX, sizeY, i, properties));
        }

        return maps;
    }
}

module.exports = Map;