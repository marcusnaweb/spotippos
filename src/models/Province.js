'use strict';
let Checks = require('../utils/Checks');
let ZoneBounds = require('./ZoneBounds');
let Area = require('./Area');
let Storage = require('../storage/Local');

class Province extends Area {
    constructor(ax, ay, bx, by, name) {
        super(ax, ay, bx, by);
        Checks.requireNonNull(name, 'name');

        this.name = name;
        Object.freeze(this);
    }

    static load(storageURI) {
        return Storage.load(storageURI)
               .then(rawProvinces => Province.parseRawObject(rawProvinces));
        
    }
   
    static loadSync(storageURI) {
        let rawProvinces = Storage.loadSync(storageURI);
        return Province.parseRawObject(rawProvinces);
    }

    static parseRawObject(rawObject) {
        let provinces = [];

        for (let key in rawObject) {
            let boundaries = rawObject[key].boundaries;

            provinces.push(new Province(boundaries.upperLeft.x,
                boundaries.upperLeft.y,
                boundaries.bottomRight.x,
                boundaries.bottomRight.y, key));
        }

        return provinces;
    }

    toJSON() {
        return this.name;
    }
}

module.exports = Province;