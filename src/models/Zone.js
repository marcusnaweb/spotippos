'use strict';
let Checks = require('../utils/Checks');
let Area = require('./Area');

class Zone extends Area {
    constructor(ax, ay, bx, by, properties) {
        super(ax, ay, bx, by);
        Checks.requireNonNull(properties, 'properties');

        this.properties = properties.filter(property => property.isIn(this.zoneBounds));

        Object.freeze(this);
    }

    createSubZone(ax, ay, bx, by) {
        return new Zone(ax, ay, bx, by, this.properties);
    }

    static splitMap(map, zonesCount, properties) {
        Checks.requireNonNull(properties, 'properties');
        Checks.requireNonNull(map, 'map');
        Checks.requirePositive(zonesCount, 'zonesCount');

        let ax = 0, ay = 0, bx = 0, by = 0;
        let zones = [];
        let columnIndex = 0;
        let columnHeight = 0;
        
        do {
            ax = map.sizeX / zonesCount * columnIndex;
            ay = map.sizeY;
            bx = ax + map.sizeX / zonesCount;
            by = columnHeight;
            zones.push(new Zone(ax, ay, bx, by, properties));
            columnIndex++;
        } while (zonesCount > columnIndex);

        return zones;
    }

    toJSON() {
        return {
            totalProperties: this.properties.length,
            properties: this.properties
        };
    }
}

module.exports = Zone;