'use strict';
let glob = require('glob');
let callsite = require('callsite');
let path = require('path');
let Checks = require('./Checks');

class RouteLoader {
    static load(paths) {
        Checks.requireArray(paths, 'paths');
        let stack = callsite();
        let cwd = path.dirname(stack[1].getFileName());

        return paths.map(path => glob.sync(path, { cwd: cwd }))
            .reduce((filepaths, current) => filepaths.concat(current), [])
            .map(filepath => ({
                path: `/${path.basename(filepath, '.js')}`,
                router: require(path.join(cwd, filepath))
            }));
    }
}

module.exports = RouteLoader;