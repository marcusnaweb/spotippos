'use strict';

class Checks {
    static requireNonNull(obj, name) {
        if (obj === null || obj == undefined)
            throw new Error(`O parâmetro ${name} não pode ser null ou undefined.`)
    }

    static requirePositive(n, name) {
        Checks.requireNumber(n, name);
        
        if (n <= 0)
            throw new Error(`O parâmetro ${name} deve ser um número positivo.`);
    }

    static requireArray(array, name) {
        if (!Array.isArray(array))
            throw new Error(`O parâmetro ${name} deve ser um array.`);
    }

    static requireNumber(n, name) {
        if (isNaN(n) || typeof n !== 'number')
            throw new Error(`O parâmetro ${name} deve ser um número.`);
    }

    static instanceOf(obj, type, name) {
        if (!(obj instanceof type))
            throw new Error(`O parâmetro ${name} deve ser um ${type.name}`);
    }
}

module.exports = Checks;