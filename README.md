## Instruções
### Requisitos de software:
  - [**docker 1.12**](https://docs.docker.com/engine/installation/)
  - [**docker-compose**](https://docs.docker.com/compose/install/)
  - [**nodejs 6.3.3**](https://nodejs.org/en/blog/release/v6.3.0/)

# Comandos
#### Rodando com o docker-compose:
Ao rodar com o Docker é possivel ver a especificação da API no [Swagger Editor](http://localhost:3001/) e a telemetria no [Grafana](http://localhost:3001/) (username: admin password: admin).

```bash
docker-compose up
```
#### Rodando localmente:
```bash
npm install
npm start
```

#### Rodando testes:
```bash
npm install
npm test
```

#### Troubleshooting:
  - "ERROR: for app  No such image:" a tentar executar docker-compose up.
    - Executar ```docker-compose rm``` digitar y e depois ```docker-compose``` novamente.
  - "Estou no Windows e está tudo rodando mas não consigo acessar via 127.0.0.1":
    - Abra o Docker Quickstart Terminal e digite ```docker-machine ip``` e utilize o endereço que aparecer.