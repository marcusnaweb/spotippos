'use strict';
let gulp = require('gulp');
let watch = require('gulp-watch');
let mocha = require('gulp-mocha');

const specPath = './spec/**/*.Spec.js';
const sourcePath = './src/**/*.js';

gulp.task('default', ['test'], () => {
    watch([specPath, sourcePath], () => {
        gulp.start('test');
    });
});

gulp.task('test', function () {
    return gulp.src(specPath)
        .pipe(mocha());
});